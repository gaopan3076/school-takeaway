from marshmallow import fields, validate

from takeaway import const
from takeaway.schema import BaseSchema


class User(BaseSchema):
    id = fields.Int(dump_only=True)
    nickname = fields.Str(required=True)
    phone = fields.Str(required=True, validate=validate.Length(equal=11))
    gender = fields.Enum(const.Gender, required=True)
    password = fields.Str(required=True, validate=validate.Length(min=6), load_only=True)
    avatar = fields.Str(dump_only=True)


class Login(BaseSchema):
    phone = fields.Str(required=True)
    password = fields.Str(required=True)
