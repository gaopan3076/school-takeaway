from marshmallow.schema import Schema

from takeaway import util


class BaseSchema(Schema):

    def load_json(self, many=None, partial=None, unknown=None):
        return self.load(util.location_data(), many=many, partial=partial, unknown=unknown)
