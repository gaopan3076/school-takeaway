from flask import Blueprint, request
from flask_jwt_extended import jwt_required, current_user

from takeaway.service import address as service

addresses_api = Blueprint("addresses", __name__, url_prefix="/addresses")


@addresses_api.get("")
@jwt_required()
def get_addresses():
    return service.get_addresses(current_user.id)


@addresses_api.post("")
@jwt_required()
def post_addresses():
    address_name = request.json["name"]
    address_phone = request.json["phone"]
    address_place = request.json["place"]
    return service.add_address(current_user.id, address_name, address_phone, address_place)


@addresses_api.put("/<int:address_id>")
@jwt_required()
def put_addresses(address_id):
    return service.update_address(current_user.id, address_id, **(request.json or {}))
