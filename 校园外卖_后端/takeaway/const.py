import enum


@enum.unique
class Gender(enum.Enum):
    MALE = "MALE"
    FEMALE = "FEMALE"
    SECRECY = "SECRECY"
