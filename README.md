# Uniapp+Flask前后端分离项目_校园外卖

## 一、前言

#### 介绍
“校园外卖”是一个基于Uniapp + Flask技术栈实现前后端分离的开源项目，此项目中包括注册、登录、忘记密码、个人中心、店铺（店铺信息+商品）、购物车、订单、评价等系列丰富的功能. 伯仲云团队将为每一位正在学习的程序员提供一份高质量的开源项目+教学视频。

#### 体验

H5体验网址：<br>
http://www.takeaway.yunjinkechuang.com

H5体验二维码：<br>
<img src="http://www.takeaway.yunjinkechuang.com/H5_code.png" width="200" height="200"/>

#### 页面展示
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E9%A6%96%E9%A1%B5.png" width="240" height="500"/>
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E5%95%86%E5%AE%B6%E7%82%B9%E9%A4%90.png" width="240" height="500"/>
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E7%99%BB%E5%BD%95.png" width="240" height="500"/>
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E4%B8%AA%E4%BA%BA%E8%B5%84%E6%96%99.png" width="250" height="500"/>
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83-%E5%B7%B2%E7%99%BB%E5%BD%95.png" width="240" height="500"/>
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E7%BB%93%E7%AE%97%E9%A1%B5%E9%9D%A2.png" width="240" height="500"/>

#### 软件架构

UI图：即使设计（https://js.design/f/AAysZH?p=dPWq6X）

前端语言：Uniapp(https://uniapp.dcloud.net.cn/)

后端语言：Flask

数据库：mysql

## 二、前端技术讲解
#### 前期准备
1. 删除框架本身自带的组件/页面
2. pages.json配置文件中,对"globalStyle"进行配置，参考Uniapp官网，取消默认的导航栏
3. 根据设计稿确定底部导航栏tabbar的方案 <br>
（1）如果tabbar是普通且标准的样式，那么我们则在pages.json中配置tabbar选项 <br>
（2）如果tabbar是高端且独特的样式风格，那么我们会创建一个components文件夹（置放公共组件），建立tabbar公共组件进行复用 <br>
4. manifest.json文件是应用的配置文件，用于指定应用的名称、图标、权限等。HBuilderX 创建的工程此文件在根目录，CLI 创建的工程此文件在 src 目录。

#### 文件目录
<img src="https://gitee.com/gaopan3076/school-takeaway/raw/master/ui_display/%E6%A0%A1%E5%9B%AD%E5%A4%96%E5%8D%96-%E7%9B%AE%E5%BD%95.png" height="600"/>

#### tabBar公共组件
```
<template>
	<view class="tabBar">
		<view class="tabBar_list">
			<view @click="toTab('/pages/index/index')" :class="current==1?'active':''">
				<image :src="current==1?'/static/image/homeActive.svg':'/static/image/home.svg'" mode="aspectFill">
				</image>
				<text>首页</text>
			</view>
			<view @click="toTab('/pages/order/order')" :class="current==2?'active':''">
				<image :src="current==2?'/static/image/orderActive.svg':'/static/image/order.svg'" mode="aspectFill">
				</image>
				<text>订单</text>
			</view>
			<view @click="toTab('/pages/memberCenter/memberCenter')" :class="current==3?'active':''">
				<image :src="current==3?'/static/image/memberActive.svg':'/static/image/member.svg'" mode="aspectFill"></image>
				<text>我的</text>
			</view>
		</view>
	</view>
</template>

<script>
	export default {
		name: "tabBar",
		props: {
			current: {
				type: [String, Number],
				default: 1
			}
		},
		data() {
			return {

			};
		},
		methods: {
			toTab(event) {
				uni.reLaunch({
					url: event
				})
			}
		}
	}
</script>

<style lang="scss">
	.tabBar {
		position: fixed;
		bottom: 0;
		left: 0;
		height: 166rpx;
		width: 100%;
		z-index: 999;
		display: flex;
		align-items: center;
		box-sizing: border-box;
		border-top: 1rpx solid #B4BCC6;
		background-color: #fff;
		.tabBar_list {
			position: relative;
			z-index: 101;
			height: 120rpx;
			display: flex;
			justify-content: space-between;
			align-items: center;
			padding: 0 100rpx;
			width: 100%;
			>view {
				display: flex;
				flex-direction: column;
				align-items: center;
				
				&:first-child{
					image{
						width: 44rpx;
						height: 44rpx;
					}
				}
				image {
					width: 40rpx;
					height: 44rpx;
					margin-bottom: 8rpx;
				}

				text {
					font-size: 24rpx;
					color: #B4BCC6;
				}

				&.active {
					text {
						color: #2588FF;
					}
				}
			}
		}
	}
</style>
```

页面讲解请至哔哩哔哩（）观看详情课程
#### 静态页面开发
##### 页面第一节：tabBar+首页+订单+我的
##### 页面第二节：登录+注册+个人资料+忘记密码+我的地址+添加地址+编辑地址
##### 页面第三节：搜索+店铺列表+店铺详情
##### 页面第四节：结算+支付+订单详情+商品评价
## 三、后端技术讲解


