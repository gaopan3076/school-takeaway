
from flask import make_response


def __generic(ok, msg, data=None):
    return dict(ok=ok, msg=msg, data=data)


def make(ok, msg, data=None, status_code=200, headers=None):
    return make_response(__generic(ok, msg, data), status_code, headers)


def make_success(msg="success", data=None, status_code=200, headers=None):
    return make(True, msg, data, status_code, headers)


def make_fail(msg="fail", data=None, status_code=200, headers=None):
    return make(False, msg, data, status_code, headers)
