import os

from flask import Flask

from takeaway import ext
from takeaway.api import api


def create_app():
    app = Flask(__name__)

    # set config
    app.config["JWT_SECRET_KEY"] = "random"
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{os.path.dirname(app.root_path)}/data.sqlite3"

    # init plugins
    ext.db.init_app(app)
    ext.migrate.init_app(app)
    ext.jwt.init_app(app)

    # register blueprints
    app.register_blueprint(api)

    return app
