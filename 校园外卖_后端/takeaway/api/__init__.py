from flask import Blueprint
from marshmallow.exceptions import ValidationError

from takeaway import resp

api = Blueprint("api", __name__, url_prefix="/api")


@api.errorhandler(415)
def errorhandler415(error):
    return resp.make_fail(msg=error.description, status_code=error.code)


@api.errorhandler(ValidationError)
def errorhandler_validation(error):
    print(error)
    return resp.make_fail(msg="validation error", data=error.messages)


from takeaway.api.user import user_api  # noqa
from takeaway.api.address import addresses_api  # noqa

api.register_blueprint(user_api)
api.register_blueprint(addresses_api)
