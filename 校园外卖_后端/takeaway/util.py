from flask import request


def location_data(location="json"):
    data = {}
    match location:
        case "json":
            data = request.json
        case "form":
            data = request.form
        case "args":
            data = request.args
        case "headers":
            data = request.headers
        case _:
            pass
    return data


def args(*names, defaults=None, location="json"):
    defaults = defaults or {}
    data = location_data(location)

    values = []
    for name in names:
        if name in data:
            values.append(data[name])
        elif name in defaults:
            values.append(defaults[name])
        else:
            raise MissingArgumentException(name)

    if len(values) == 1:
        return values[0]

    return values
