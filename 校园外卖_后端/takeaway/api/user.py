from flask import Blueprint

from takeaway.ext import jwt
from takeaway.store import user as store
from takeaway.service import user as service
from takeaway.schema import user as schema

user_api = Blueprint("user", __name__, url_prefix="/users")


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return store.select_user_by_id(identity)


# 注册用户
@user_api.post("/signup")
def signup():
    return service.add_user(schema.User().load_json())


# 登录
@user_api.post("/signin")
def login():
    return service.login(schema.Login().load_json())
